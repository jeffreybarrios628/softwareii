namespace TareaCurso
{
    partial class FrmRegistroPóliza
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtBuscarVehic = new System.Windows.Forms.Button();
            this.txtBuscarVivi = new System.Windows.Forms.Button();
            this.txtBuscarVida = new System.Windows.Forms.Button();
            this.btnBuscarAseg = new System.Windows.Forms.Button();
            this.btnBuscarCont = new System.Windows.Forms.Button();
            this.mskPoliza = new System.Windows.Forms.MaskedTextBox();
            this.txtVehi = new System.Windows.Forms.TextBox();
            this.txtVivienda = new System.Windows.Forms.TextBox();
            this.txtVida = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtAseg = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtContra = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtid = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.cbAsegurado = new System.Windows.Forms.CheckBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.flowLayoutPanel2);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.flowLayoutPanel1);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(676, 335);
            this.panel1.TabIndex = 3;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.btnCancelar);
            this.flowLayoutPanel2.Controls.Add(this.btnGuardar);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(12, 292);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.flowLayoutPanel2.Size = new System.Drawing.Size(652, 33);
            this.flowLayoutPanel2.TabIndex = 6;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelar.Location = new System.Drawing.Point(560, 3);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(89, 23);
            this.btnCancelar.TabIndex = 24;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGuardar.Location = new System.Drawing.Point(465, 3);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(89, 23);
            this.btnGuardar.TabIndex = 23;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.BtnGuardar_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.txtBuscarVehic);
            this.groupBox2.Controls.Add(this.txtBuscarVivi);
            this.groupBox2.Controls.Add(this.txtBuscarVida);
            this.groupBox2.Controls.Add(this.btnBuscarAseg);
            this.groupBox2.Controls.Add(this.btnBuscarCont);
            this.groupBox2.Controls.Add(this.mskPoliza);
            this.groupBox2.Controls.Add(this.txtVehi);
            this.groupBox2.Controls.Add(this.txtVivienda);
            this.groupBox2.Controls.Add(this.txtVida);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.txtAseg);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtContra);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtid);
            this.groupBox2.Controls.Add(this.label27);
            this.groupBox2.Controls.Add(this.label26);
            this.groupBox2.Controls.Add(this.cbAsegurado);
            this.groupBox2.Location = new System.Drawing.Point(12, 97);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(652, 189);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Datos del cliente";
            // 
            // txtBuscarVehic
            // 
            this.txtBuscarVehic.Location = new System.Drawing.Point(494, 55);
            this.txtBuscarVehic.Name = "txtBuscarVehic";
            this.txtBuscarVehic.Size = new System.Drawing.Size(141, 23);
            this.txtBuscarVehic.TabIndex = 81;
            this.txtBuscarVehic.Text = "Vehiculos";
            this.txtBuscarVehic.UseVisualStyleBackColor = true;
            this.txtBuscarVehic.Click += new System.EventHandler(this.TxtBuscarVehic_Click);
            // 
            // txtBuscarVivi
            // 
            this.txtBuscarVivi.Location = new System.Drawing.Point(494, 85);
            this.txtBuscarVivi.Name = "txtBuscarVivi";
            this.txtBuscarVivi.Size = new System.Drawing.Size(141, 23);
            this.txtBuscarVivi.TabIndex = 80;
            this.txtBuscarVivi.Text = "Viviendas";
            this.txtBuscarVivi.UseVisualStyleBackColor = true;
            this.txtBuscarVivi.Click += new System.EventHandler(this.TxtBuscarVivi_Click);
            // 
            // txtBuscarVida
            // 
            this.txtBuscarVida.Location = new System.Drawing.Point(494, 118);
            this.txtBuscarVida.Name = "txtBuscarVida";
            this.txtBuscarVida.Size = new System.Drawing.Size(141, 23);
            this.txtBuscarVida.TabIndex = 79;
            this.txtBuscarVida.Text = "Beneficiarios";
            this.txtBuscarVida.UseVisualStyleBackColor = true;
            this.txtBuscarVida.Click += new System.EventHandler(this.TxtBuscarVida_Click);
            // 
            // btnBuscarAseg
            // 
            this.btnBuscarAseg.Location = new System.Drawing.Point(205, 118);
            this.btnBuscarAseg.Name = "btnBuscarAseg";
            this.btnBuscarAseg.Size = new System.Drawing.Size(100, 23);
            this.btnBuscarAseg.TabIndex = 78;
            this.btnBuscarAseg.Text = "Asegurados";
            this.btnBuscarAseg.UseVisualStyleBackColor = true;
            this.btnBuscarAseg.Click += new System.EventHandler(this.BtnBuscarAseg_Click);
            // 
            // btnBuscarCont
            // 
            this.btnBuscarCont.Location = new System.Drawing.Point(205, 92);
            this.btnBuscarCont.Name = "btnBuscarCont";
            this.btnBuscarCont.Size = new System.Drawing.Size(100, 23);
            this.btnBuscarCont.TabIndex = 77;
            this.btnBuscarCont.Text = "Contratantes";
            this.btnBuscarCont.UseVisualStyleBackColor = true;
            this.btnBuscarCont.Click += new System.EventHandler(this.BtnBuscarCont_Click);
            // 
            // mskPoliza
            // 
            this.mskPoliza.Location = new System.Drawing.Point(107, 66);
            this.mskPoliza.Mask = ">L>L-999999-9";
            this.mskPoliza.Name = "mskPoliza";
            this.mskPoliza.Size = new System.Drawing.Size(120, 20);
            this.mskPoliza.TabIndex = 76;
            // 
            // txtVehi
            // 
            this.txtVehi.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtVehi.Enabled = false;
            this.txtVehi.Location = new System.Drawing.Point(383, 58);
            this.txtVehi.Name = "txtVehi";
            this.txtVehi.Size = new System.Drawing.Size(105, 20);
            this.txtVehi.TabIndex = 75;
            // 
            // txtVivienda
            // 
            this.txtVivienda.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtVivienda.Enabled = false;
            this.txtVivienda.Location = new System.Drawing.Point(383, 91);
            this.txtVivienda.Name = "txtVivienda";
            this.txtVivienda.Size = new System.Drawing.Size(105, 20);
            this.txtVivienda.TabIndex = 74;
            // 
            // txtVida
            // 
            this.txtVida.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtVida.Enabled = false;
            this.txtVida.Location = new System.Drawing.Point(383, 121);
            this.txtVida.Name = "txtVida";
            this.txtVida.Size = new System.Drawing.Size(105, 20);
            this.txtVida.TabIndex = 73;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(326, 61);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 72;
            this.label7.Text = "Vehiculo:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(326, 94);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 13);
            this.label6.TabIndex = 71;
            this.label6.Text = "Vivienda:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(339, 124);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 70;
            this.label5.Text = "Vida:";
            // 
            // txtAseg
            // 
            this.txtAseg.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtAseg.Enabled = false;
            this.txtAseg.Location = new System.Drawing.Point(107, 118);
            this.txtAseg.Name = "txtAseg";
            this.txtAseg.Size = new System.Drawing.Size(92, 20);
            this.txtAseg.TabIndex = 69;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(40, 121);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 68;
            this.label4.Text = "Asegurado:";
            // 
            // txtContra
            // 
            this.txtContra.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtContra.Enabled = false;
            this.txtContra.Location = new System.Drawing.Point(107, 92);
            this.txtContra.Name = "txtContra";
            this.txtContra.Size = new System.Drawing.Size(92, 20);
            this.txtContra.TabIndex = 67;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(46, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 65;
            this.label3.Text = "No Póliza:";
            // 
            // txtid
            // 
            this.txtid.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtid.Enabled = false;
            this.txtid.Location = new System.Drawing.Point(85, 23);
            this.txtid.Name = "txtid";
            this.txtid.Size = new System.Drawing.Size(57, 20);
            this.txtid.TabIndex = 63;
            // 
            // label27
            // 
            this.label27.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(58, 26);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(21, 13);
            this.label27.TabIndex = 64;
            this.label27.Text = "ID:";
            this.label27.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(36, 95);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(65, 13);
            this.label26.TabIndex = 57;
            this.label26.Text = "Contratante:";
            // 
            // cbAsegurado
            // 
            this.cbAsegurado.AutoSize = true;
            this.cbAsegurado.Location = new System.Drawing.Point(431, 15);
            this.cbAsegurado.Name = "cbAsegurado";
            this.cbAsegurado.Size = new System.Drawing.Size(172, 30);
            this.cbAsegurado.TabIndex = 55;
            this.cbAsegurado.Text = "Marcar SI el Contratante \r\ny el Asegurado son los mismos.";
            this.cbAsegurado.UseVisualStyleBackColor = true;
            this.cbAsegurado.CheckedChanged += new System.EventHandler(this.CbAsegurado_CheckedChanged);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.button2);
            this.flowLayoutPanel1.Controls.Add(this.button1);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(18, 613);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.flowLayoutPanel1.Size = new System.Drawing.Size(646, 27);
            this.flowLayoutPanel1.TabIndex = 4;
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(554, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(89, 23);
            this.button2.TabIndex = 21;
            this.button2.Text = "Cancelar";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(459, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(89, 23);
            this.button1.TabIndex = 20;
            this.button1.Text = "Guardar";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(68)))), ((int)(((byte)(173)))));
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(676, 87);
            this.panel2.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox1.Location = new System.Drawing.Point(12, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(652, 81);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::TareaCurso.Properties.Resources.sVida;
            this.pictureBox2.Location = new System.Drawing.Point(569, 9);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(77, 66);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::TareaCurso.Properties.Resources.CheckMark;
            this.pictureBox1.Location = new System.Drawing.Point(6, 9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(94, 66);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(239, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(169, 21);
            this.label2.TabIndex = 2;
            this.label2.Text = "Datos del Asegurado";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(124, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(414, 26);
            this.label1.TabIndex = 1;
            this.label1.Text = "Formato de solicitud de Póliza de seguros";
            // 
            // FrmRegistroPóliza
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(676, 335);
            this.Controls.Add(this.panel1);
            this.Name = "FrmRegistroPóliza";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmRegistroPóliza";
            this.panel1.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.CheckBox cbAsegurado;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.TextBox txtid;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.MaskedTextBox mskPoliza;
        private System.Windows.Forms.TextBox txtVehi;
        private System.Windows.Forms.TextBox txtVivienda;
        private System.Windows.Forms.TextBox txtVida;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtAseg;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtContra;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button txtBuscarVehic;
        private System.Windows.Forms.Button txtBuscarVivi;
        private System.Windows.Forms.Button txtBuscarVida;
        private System.Windows.Forms.Button btnBuscarAseg;
        private System.Windows.Forms.Button btnBuscarCont;
    }
}