using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TareaCurso
{
    public partial class FrmAsegurados : Form
    {
        public FrmAsegurados()
        {
            InitializeComponent();
        }

        private void FrmAsegurados_Load(object sender, EventArgs e)
        {
            string cmd = "select*from Asegurado";
            DataSet ds = Utilidades.Ejecutar(cmd);
            dataGridView1.DataSource = ds.Tables[0];
        }

        private void BtnColocar_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();

        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {

                DataSet ds;

                string cmd = "select * from Asegurado where Primer_Nombre Like ('%" + txtbuscar.Text + "%')";
                ds = Utilidades.Ejecutar(cmd);
                dataGridView1.DataSource = ds.Tables[0];


            }
            catch (Exception error)
            {
                MessageBox.Show("Ocurrió un error ", error.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void Btnadd_Click(object sender, EventArgs e)
        {
            FrmRegistroAsegurado aseg = new FrmRegistroAsegurado();
            aseg.Show();
        }

        private void Btndelete_Click(object sender, EventArgs e)
        {
            try
            {

                DialogResult res = MessageBox.Show("¿Seguro que desea eliminar a este usuario?", "Atención",
                   MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (res == DialogResult.Yes)
                {
                    int fila = Convert.ToInt32(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value.ToString());
                    string cmd = string.Format("exec EliminarAsegurado '{0}'", fila);
                    Utilidades.Ejecutar(cmd);
                    MessageBox.Show("Eliminado con éxito", "Completado");
                    cmd = string.Format("select * from Asegurado");
                    DataSet ds = Utilidades.Ejecutar(cmd);
                    dataGridView1.DataSource = ds.Tables[0];
                }
            }
            catch (Exception error)
            {

                MessageBox.Show("Ocurrió un error ", error.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

        }

        private void DataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            txtEstado.Text = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[6].Value.ToString();
            txtDireccion.Text = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[7].Value.ToString();
            msktele.Text = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[10].Value.ToString();
            mskCel.Text = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[9].Value.ToString();
            txtLTrabajo.Text = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[19].Value.ToString();
            txtCorreo.Text = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[11].Value.ToString();
        }

        private void Btnupdate_Click(object sender, EventArgs e)
        {
            string id = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();
            string cmd = string.Format("exec ActualizarAsegurado '{0}','{1}','{2}','{3}','{4}','{5}','{6}'",
            id, txtDireccion.Text, txtEstado.Text, mskCel.Text, msktele.Text, txtCorreo.Text, txtLTrabajo.Text);

            Utilidades.Ejecutar(cmd);
            MessageBox.Show("Se Actualizó correctamente");
            string cmd1 = "select*from Asegurado";
            DataSet ds = Utilidades.Ejecutar(cmd1);
            dataGridView1.DataSource = ds.Tables[0];
        }

        private void Btnact_Click(object sender, EventArgs e)
        {
            string cmd1 = "select*from Asegurado";
            DataSet ds = Utilidades.Ejecutar(cmd1);
            dataGridView1.DataSource = ds.Tables[0];
        }
    }
}
