using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace TareaCurso
{
    public partial class FrmRegistroCliente : Form
    {
    //    Conection connect = new Conection("Yarred", "Yadersito");

        int fila;

        public FrmRegistroCliente()
        {
            InitializeComponent();
        }

        private void FrmRegistroCliente_Load(object sender, EventArgs e)
        {
            
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            string seleccionado = "";
             foreach(Control contr in gbSexo.Controls)
            {

                if(contr is RadioButton)
                {
                    RadioButton rad = contr as RadioButton;
                    if (rad.Checked)
                    {
                        
                        seleccionado = rad.Text;
                        break;

                    }

                }
            }

            try
            {

                    string cmd = string.Format("exec AgregarCliente '{0}','{1}','{2}','{3}','{4}'," +
                    "'{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}'," +
                    "'{16}','{17}','{18}','{19}'", txtPNombre.Text, txtPApellido.Text, txtSNombre.Text, txtSApellido.Text,
                    seleccionado, cmbECivil.SelectedItem.ToString(), txtDireccion.Text, txtMuni.Text, mskCel.Text, msktel.Text,
                    txtCorreo.Text, txtNacionalidad.Text, mskCedu.Text, txtPasaporte.Text, txtCResidencia.Text,
                    txtDocCA4.Text, txtProfesion.Text, cmbActEcon.Text.ToString(), txtLTrabajo.Text, txtCargo.Text
                    );

                Utilidades.Ejecutar(cmd);
                MessageBox.Show("Se agregó correctamente", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
               
            }
            catch (Exception error)
            {

                MessageBox.Show("Ocurrió un error ", error.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnBuscar_Click(object sender, EventArgs e)
        {
           FrmMunicipios cl = new FrmMunicipios();
            DialogResult = DialogResult.OK;

            cl.ShowDialog();
            if (cl.DialogResult == DialogResult.OK)
            {
                txtMuni.Text = cl.dataGridView1.Rows[cl.dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();

            }
        }

        private void TxtPNombre_KeyPress(object sender, KeyPressEventArgs e)
        {

            if(!(char.IsLetter(e.KeyChar)) && (e.KeyChar!=(char)Keys.Back))
            {

                MessageBox.Show("Sólo se permiten letras", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }

        }

        private void TxtPApellido_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {

                MessageBox.Show("Sólo se permiten letras", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void TxtSNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {

                MessageBox.Show("Sólo se permiten letras", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void TxtSApellido_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {

                MessageBox.Show("Sólo se permiten letras", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void TxtDireccion_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {

                MessageBox.Show("Sólo se permiten letras", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void TxtProfesion_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {

                MessageBox.Show("Sólo se permiten letras", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void TxtLTrabajo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {

                MessageBox.Show("Sólo se permiten letras", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void TxtNacionalidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {

                MessageBox.Show("Sólo se permiten letras", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void TxtCargo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {

                MessageBox.Show("Sólo se permiten letras", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }
    }
}
