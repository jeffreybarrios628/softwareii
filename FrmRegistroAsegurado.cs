using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TareaCurso
{
    public partial class FrmRegistroAsegurado : Form
    {
        string cmd = "";
        public FrmRegistroAsegurado()
        {
            InitializeComponent();
        }

        private void CbAsegurado_CheckedChanged(object sender, EventArgs e)
        {
            FrmCliente cl = new FrmCliente();
            cl.ShowDialog();
            


            if (cl.DialogResult == DialogResult.OK)
            {

            txtPNombre.Text = cl.dataGridView1.Rows[cl.dataGridView1.CurrentRow.Index].Cells[1].Value.ToString();
            txtSNombre.Text = cl.dataGridView1.Rows[cl.dataGridView1.CurrentRow.Index].Cells[2].Value.ToString();
            txtPApellido.Text = cl.dataGridView1.Rows[cl.dataGridView1.CurrentRow.Index].Cells[3].Value.ToString();
            txtSApellido.Text = cl.dataGridView1.Rows[cl.dataGridView1.CurrentRow.Index].Cells[4].Value.ToString();
            string selec = cl.dataGridView1.Rows[cl.dataGridView1.CurrentRow.Index].Cells[5].Value.ToString();
            if (selec == "Masculino")
            {
                rbtnMasculino.Checked = true;
            }
            else
            {
                rbtnFemenino.Checked = true;
            }
            cmbECivil.Text = cl.dataGridView1.Rows[cl.dataGridView1.CurrentRow.Index].Cells[6].Value.ToString();
            txtDireccion.Text = cl.dataGridView1.Rows[cl.dataGridView1.CurrentRow.Index].Cells[7].Value.ToString();
            txtMuni.Text = cl.dataGridView1.Rows[cl.dataGridView1.CurrentRow.Index].Cells[8].Value.ToString();
            mskCel.Text = cl.dataGridView1.Rows[cl.dataGridView1.CurrentRow.Index].Cells[9].Value.ToString();
            msktel.Text = cl.dataGridView1.Rows[cl.dataGridView1.CurrentRow.Index].Cells[10].Value.ToString();
            txtCorreo.Text = cl.dataGridView1.Rows[cl.dataGridView1.CurrentRow.Index].Cells[11].Value.ToString();
            txtNacionalidad.Text = cl.dataGridView1.Rows[cl.dataGridView1.CurrentRow.Index].Cells[12].Value.ToString();
            mskCedu.Text = cl.dataGridView1.Rows[cl.dataGridView1.CurrentRow.Index].Cells[13].Value.ToString();
            txtPasaporte.Text = cl.dataGridView1.Rows[cl.dataGridView1.CurrentRow.Index].Cells[14].Value.ToString();
            txtCResidencia.Text = cl.dataGridView1.Rows[cl.dataGridView1.CurrentRow.Index].Cells[15].Value.ToString();
            txtDocCA4.Text = cl.dataGridView1.Rows[cl.dataGridView1.CurrentRow.Index].Cells[16].Value.ToString();
            txtProfesion.Text = cl.dataGridView1.Rows[cl.dataGridView1.CurrentRow.Index].Cells[17].Value.ToString();
            cmbActEcon.Text = cl.dataGridView1.Rows[cl.dataGridView1.CurrentRow.Index].Cells[18].Value.ToString();
            txtLTrabajo.Text = cl.dataGridView1.Rows[cl.dataGridView1.CurrentRow.Index].Cells[19].Value.ToString();
            txtCargo.Text = cl.dataGridView1.Rows[cl.dataGridView1.CurrentRow.Index].Cells[20].Value.ToString();

        }
            
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
          
            
                string seleccionado = "";
                foreach (Control contr in gbSexo.Controls)
                {

                    if (contr is RadioButton)
                    {
                        RadioButton rad = contr as RadioButton;
                        if (rad.Checked)
                        {

                            seleccionado = rad.Text;
                            break;

                        }

                    }
                }

            if (cbAsegurado.Checked == true) { 

                try
                {


                    cmd = string.Format("exec AgregarAsegurado '{0}','{1}','{2}','{3}','{4}'," +
                   "'{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}'," +
                   "'{16}','{17}','{18}','{19}'", txtPNombre.Text.Trim(), txtSNombre.Text.Trim(), txtPApellido.Text.Trim(), txtSApellido.Text.Trim(),
                   seleccionado, cmbECivil.Text.ToString(), txtDireccion.Text.Trim(), txtMuni.Text.Trim(), mskCel.Text.Trim(), msktel.Text.Trim(),
                   txtCorreo.Text.Trim(), txtNacionalidad.Text.Trim(), mskCedu.Text.Trim(), txtPasaporte.Text.Trim(), txtCResidencia.Text.Trim(),
                   txtDocCA4.Text.Trim(), txtProfesion.Text.Trim(), cmbActEcon.Text.ToString(), txtLTrabajo.Text.Trim(), txtCargo.Text.Trim()
                   );

                    Utilidades.Ejecutar(cmd);
                    MessageBox.Show("Se agregó correctamente", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception error)
                {

                    MessageBox.Show("Ocurrió un error ", error.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }
            else
            {
                try
                {


                    cmd = string.Format("exec AgregarAsegurado '{0}','{1}','{2}','{3}','{4}'," +
                   "'{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}'," +
                   "'{16}','{17}','{18}','{19}'", txtPNombre.Text.Trim(), txtSNombre.Text.Trim(), txtPApellido.Text.Trim(), txtSApellido.Text.Trim(),
                   seleccionado, cmbECivil.SelectedItem.ToString(), txtDireccion.Text.Trim(), txtMuni.Text.Trim(), mskCel.Text.Trim(), msktel.Text.Trim(),
                   txtCorreo.Text.Trim(), txtNacionalidad.Text.Trim(), mskCedu.Text.Trim(), txtPasaporte.Text.Trim(), txtCResidencia.Text.Trim(),
                   txtDocCA4.Text.Trim(), txtProfesion.Text.Trim(), cmbActEcon.SelectedItem.ToString(), txtLTrabajo.Text.Trim(), txtCargo.Text.Trim()
                   );

                    Utilidades.Ejecutar(cmd);
                    MessageBox.Show("Se agregó correctamente", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception error)
                {

                    MessageBox.Show("Ocurrió un error ", error.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);

                }

            }
                 
            }

        private void BtnBuscar_Click(object sender, EventArgs e)
        {
            FrmMunicipios cl = new FrmMunicipios();
            DialogResult = DialogResult.OK;

            cl.ShowDialog();
            if (cl.DialogResult == DialogResult.OK)
            {
                txtMuni.Text = cl.dataGridView1.Rows[cl.dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();

            }
        }

        private void TxtPNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {

                MessageBox.Show("Sólo se permiten letras", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void TxtPApellido_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {

                MessageBox.Show("Sólo se permiten letras", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void TxtSNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {

                MessageBox.Show("Sólo se permiten letras", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void TxtSApellido_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {

                MessageBox.Show("Sólo se permiten letras", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void TxtProfesion_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {

                MessageBox.Show("Sólo se permiten letras", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void TxtLTrabajo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {

                MessageBox.Show("Sólo se permiten letras", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void TxtNacionalidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {

                MessageBox.Show("Sólo se permiten letras", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void TxtCargo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {

                MessageBox.Show("Sólo se permiten letras", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }
    }
    }


