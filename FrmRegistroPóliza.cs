using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TareaCurso
{
    public partial class FrmRegistroPóliza : Form
    {
        public FrmRegistroPóliza()
        {
            InitializeComponent();
        }

        private void Label11_Click(object sender, EventArgs e)
        {

        }

        private void TxtDireccion_TextChanged(object sender, EventArgs e)
        {

        }

        private void Label8_Click(object sender, EventArgs e)
        {

        }

        private void CmbDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void CbAsegurado_CheckedChanged(object sender, EventArgs e)
        {
              if(cbAsegurado.Checked==true)
            {
                btnBuscarAseg.Enabled = false;

            }
            else
            {
                btnBuscarAseg.Enabled = true;
            }
        }

        public static DataSet fill(string tabla)
        {
            DataSet ds;
            string cmd = string.Format("exec "+tabla);
            ds = Utilidades.Ejecutar(cmd);
            return ds;

        }
        private void BtnGuardar_Click(object sender, EventArgs e)
        {

            try
            {

                string cmd = string.Format("exec IngresarPoliza '{0}','{1}','{2}','{3}','{4}','{5}'",
                txtContra.Text, txtAseg.Text, mskPoliza.Text, txtVehi.Text, txtVivienda.Text, txtVida.Text);
                Utilidades.Ejecutar(cmd);
                MessageBox.Show("Se agregó correctamente", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                FrmPóliza pol = new FrmPóliza();
                this.Hide();


            }
            catch (Exception error)
            {

                MessageBox.Show("Ocurrió un error ", error.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void BtnBuscarCont_Click(object sender, EventArgs e)
        {
            FrmCliente cl = new FrmCliente();
            DialogResult = DialogResult.OK;
         
            cl.ShowDialog();
            if (cl.DialogResult == DialogResult.OK)
            {
                txtContra.Text= cl.dataGridView1.Rows[cl.dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();

            }
        }

        private void BtnBuscarAseg_Click(object sender, EventArgs e)
        {
            FrmAsegurados cl = new FrmAsegurados();
            DialogResult = DialogResult.OK;

            cl.ShowDialog();
            if (cl.DialogResult == DialogResult.OK)
            {
                txtAseg.Text = cl.dataGridView1.Rows[cl.dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();

            }

        }

        private void TxtBuscarVehic_Click(object sender, EventArgs e)
        {
            FrmSegurosVehiculo cl = new FrmSegurosVehiculo();
            DialogResult = DialogResult.OK;

            cl.ShowDialog();
            if (cl.DialogResult == DialogResult.OK)
            {
                txtVehi.Text = cl.dataGridView1.Rows[cl.dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();

            }
        }

        private void TxtBuscarVivi_Click(object sender, EventArgs e)
        {
            FrmSegurosHogar cl = new FrmSegurosHogar();
            DialogResult = DialogResult.OK;

            cl.ShowDialog();
            if (cl.DialogResult == DialogResult.OK)
            {
                txtVivienda.Text = cl.dataGridView1.Rows[cl.dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();

            }
        }

        private void TxtBuscarVida_Click(object sender, EventArgs e)
        {
            FrmSegurosVida cl = new FrmSegurosVida();
            DialogResult = DialogResult.OK;

            cl.ShowDialog();
            if (cl.DialogResult == DialogResult.OK)
            {
                txtVida.Text = cl.dataGridView1.Rows[cl.dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();

            }
        }
    }
}
