
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TareaCurso
{
    public partial class frmLogin2 : Form
    {
        int cont = 3;

        Conection connect;
        BackgroundWorker bg = new BackgroundWorker();
        public frmLogin2()
        {
            InitializeComponent();
        }

        private void bg_DoWork(object sender, EventArgs e)
        {
            int progreso = 0, porciento = 0;


            for (int i = 0; i <= 100; i++)
            {
                progreso++;
                Thread.Sleep(50);
                bg.ReportProgress(i);
            }
        }

        private void bg_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

            progressBar1.Value = e.ProgressPercentage;
            progressBar1.Style = ProgressBarStyle.Continuous;


            if (e.ProgressPercentage > 100)
            {
                progressBar1.Value = progressBar1.Maximum;
            }
            else
            {
                progressBar1.Value = e.ProgressPercentage;
            }

        }

        private void bg_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

            MDIPrincipal parent1 = new MDIPrincipal(connect);
            parent1.Show();
            this.Hide();

        }
        private void BtnAcced_Click(object sender, EventArgs e)
        {
            if (txtUser.Text.Equals("") || txtPass.Text.Equals(""))
            {
                MessageBox.Show("CAMPOS NO PUEDEN ESTAR VACIOS. ", "Alerta", MessageBoxButtons.OK);
                Cursor.Current = Cursors.Default;
                return;
            }

            connect = new Conection(txtUser.Text, txtPass.Text);

            if (this.connect.connect.State == ConnectionState.Open)
            {
                bg.WorkerReportsProgress = true;
                bg.ProgressChanged += bg_ProgressChanged;
                bg.DoWork += bg_DoWork;
                bg.RunWorkerCompleted += bg_RunWorkerCompleted;
                bg.RunWorkerAsync();
                //codigo para hacer la cansulta y saber quien está dentro de la aplicación
                progressBar1.Visible = true;
             


            }
            else
            {
                Cursor.Current = Cursors.Default;
                --cont;
                MessageBox.Show("Error:usuario o contrasenia incorrecta ");

                if (cont == 0)

                {
                    cont = 3;
                    btnAcced.Enabled = false;
                    Thread.Sleep(3000);
                    btnAcced.Enabled = true;

                }


            }
        }

        private void PnLeft_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
