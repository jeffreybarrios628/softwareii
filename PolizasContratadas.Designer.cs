namespace TareaCurso
{
    partial class PolizasContratadas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.lblPN = new System.Windows.Forms.Label();
            this.lblSN = new System.Windows.Forms.Label();
            this.lblSA = new System.Windows.Forms.Label();
            this.lblPA = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Cliente:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataGridView1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(16, 61);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(572, 229);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Registro de polizas contratadas:";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(6, 19);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(560, 204);
            this.dataGridView1.TabIndex = 0;
            // 
            // lblPN
            // 
            this.lblPN.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPN.Location = new System.Drawing.Point(106, 13);
            this.lblPN.Name = "lblPN";
            this.lblPN.Size = new System.Drawing.Size(79, 23);
            this.lblPN.TabIndex = 2;
            this.lblPN.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSN
            // 
            this.lblSN.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSN.Location = new System.Drawing.Point(206, 13);
            this.lblSN.Name = "lblSN";
            this.lblSN.Size = new System.Drawing.Size(84, 23);
            this.lblSN.TabIndex = 3;
            this.lblSN.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSA
            // 
            this.lblSA.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSA.Location = new System.Drawing.Point(389, 13);
            this.lblSA.Name = "lblSA";
            this.lblSA.Size = new System.Drawing.Size(84, 23);
            this.lblSA.TabIndex = 4;
            this.lblSA.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPA
            // 
            this.lblPA.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPA.Location = new System.Drawing.Point(313, 13);
            this.lblPA.Name = "lblPA";
            this.lblPA.Size = new System.Drawing.Size(70, 23);
            this.lblPA.TabIndex = 5;
            this.lblPA.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PolizasContratadas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(600, 320);
            this.Controls.Add(this.lblPA);
            this.Controls.Add(this.lblSA);
            this.Controls.Add(this.lblSN);
            this.Controls.Add(this.lblPN);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Name = "PolizasContratadas";
            this.Text = "PolizasContratadas";
            this.Load += new System.EventHandler(this.PolizasContratadas_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.Label lblPN;
        public System.Windows.Forms.Label lblSN;
        public System.Windows.Forms.Label lblSA;
        public System.Windows.Forms.Label lblPA;
        public System.Windows.Forms.DataGridView dataGridView1;
    }
}