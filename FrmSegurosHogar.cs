using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TareaCurso
{
    public partial class FrmSegurosHogar : Form
    {
        public FrmSegurosHogar()
        {
            InitializeComponent();
        }

        private void BtnColocar_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void FrmSegurosHogar_Load(object sender, EventArgs e)
        {

            string cmd = string.Format("exec ListarVivienda");
            DataSet ds = Utilidades.Ejecutar(cmd);
            dataGridView1.DataSource = ds.Tables[0];

        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            FrmRegistroSHogar ho = new FrmRegistroSHogar();
            ho.Show();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            string cmd = string.Format("exec ListarVivienda");
            DataSet ds = Utilidades.Ejecutar(cmd);
            dataGridView1.DataSource = ds.Tables[0];
        }

        private void BtnActualizar_Click(object sender, EventArgs e)
        {
            string id = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();
            string cmd = string.Format("Exec ActualizarVivienda '{0}','{1}','{2}','{3}'",
                id ,txtpisos.Text, txttipo.Text, txtareac.Text);
            Utilidades.Ejecutar(cmd);
            MessageBox.Show("Se actualizó correctamente", "INFORMACION",MessageBoxButtons.OK,MessageBoxIcon.Information);
            string cmd1 = string.Format("exec ListarVivienda");
            DataSet ds = Utilidades.Ejecutar(cmd1);
            dataGridView1.DataSource = ds.Tables[0];
        }

        private void DataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            txtpisos.Text = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[3].Value.ToString();

            txttipo.Text = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[5].Value.ToString();

            txtareac.Text = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[7].Value.ToString();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            try
            {

                DialogResult res = MessageBox.Show("¿Seguro que desea eliminar a este usuario?", "Atención",
                   MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (res == DialogResult.Yes)
                {
                    int fila = Convert.ToInt32(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value.ToString());
                    string cmd = string.Format("exec EliminarVivienda '{0}'", fila);
                    Utilidades.Ejecutar(cmd);
                    MessageBox.Show("Eliminado con éxito", "Completado");
                    cmd = string.Format("exec ListarVivienda");
                    DataSet ds = Utilidades.Ejecutar(cmd);
                    dataGridView1.DataSource = ds.Tables[0];
                }
            }
            catch (Exception error)
            {

                MessageBox.Show("Ocurrió un error ", error.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }
    }
}
