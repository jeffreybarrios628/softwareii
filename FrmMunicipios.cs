using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TareaCurso
{
    public partial class FrmMunicipios : Form
    {
        public FrmMunicipios()
        {
            InitializeComponent();
        }

        private void BtnColocar_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void FrmMunicipios_Load(object sender, EventArgs e)
        {
            string cmd = "exec ListarMunicipios";
            DataSet ds = Utilidades.Ejecutar(cmd);
            dataGridView1.DataSource = ds.Tables[0];
        }

        private void Txtbuscar_TextChanged(object sender, EventArgs e)
        {
            try
            {

                DataSet ds;

                string cmd = "select * from Municipios where Municipio Like ('%" + txtbuscar.Text + "%')";
                ds = Utilidades.Ejecutar(cmd);
                dataGridView1.DataSource = ds.Tables[0];


            }
            catch (Exception error)
            {
                MessageBox.Show("Ocurrió un error ", error.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }
    }
}
