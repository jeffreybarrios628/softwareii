using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TareaCurso
{
    public partial class FrmRegistroSHogar : Form
    {
        string cmd = "";
        public FrmRegistroSHogar()
        {
            InitializeComponent();
        }

        private void Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void FrmRegistroSHogar_Load(object sender, EventArgs e)
        {
            txtUbicacion.Focus();
            txtAreaC.Enabled = true;
            txtareaTotal.Enabled = true;
            txtidbenef.Enabled = false;
            txtid.Enabled = false;
            txtmonto.Enabled = true;
            txtMuni.Enabled = true;
            txtPisos.Enabled = true;
            txtUbicacion.Enabled = true;
            cmbTipoCons.Enabled = true;
            btnCancek.Enabled = true;
            btnGuardar.Enabled = true;
            txtidbenef.Enabled = true;

        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                cmd = string.Format("EXEC AgregarVivienda '{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}'",
                  txtUbicacion.Text.Trim(), txtMuni.Text.Trim(), txtPisos.Text.Trim(), mskAño.Text.Trim(), 
                  cmbTipoCons.SelectedItem.ToString().Trim(),
                  txtareaTotal.Text.Trim(),
                  txtAreaC.Text.Trim(),
                 txtmonto.Text.Trim(),
                txtidbenef.Text.Trim()
                 ) ;
                   Utilidades.Ejecutar(cmd);
                   MessageBox.Show("Se agregó correctamente", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception error)
            {

                MessageBox.Show("Ocurrió un error ", error.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

        }

        private void CmbTipoCons_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void TxtUbicacion_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {

                MessageBox.Show("Sólo se permiten letras", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void TxtPisos_KeyPress(object sender, KeyPressEventArgs e)
        {
           
            if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar==('.')))
            {
                MessageBox.Show("Sólo se permiten números", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void TxtAreaC_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == ('.')))
            {
                MessageBox.Show("Sólo se permiten números", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void TxtareaTotal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == ('.')))
            {
                MessageBox.Show("Sólo se permiten números", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void Txtmonto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == ('.')))
            {
                MessageBox.Show("Sólo se permiten números", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }
    }
}
