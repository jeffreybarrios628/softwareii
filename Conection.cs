using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace TareaCurso
{
    public class Conection
    {
        public SqlConnection connect = new SqlConnection();

        public Conection(string user, string pass)
        {
            try
            {
                connect = new SqlConnection("Server=LAPTOP-NDG4GMRS;Database=Seguros_SA;UID=" + user + ";PWD=" + pass);
                connect.Open();
            }
            catch (Exception)
            {

            }
        }

        public void listar(DataGridView GridView1, string spListar)
        {

            SqlCommand cmd = new SqlCommand();

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = spListar;
            cmd.Connection = connect;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            DataTable dt = new DataTable();
            da.Fill(dt);

            GridView1.DataSource = dt;
        }


        public void agregar(String spAgregar, SqlParameter[] campos)
        {

            //Console.WriteLine(contra);

            try
            {
                SqlCommand cmd = new SqlCommand();

                //SqlParameter[] param = new SqlParameter[5];
                //param[0] = new SqlParameter("@Pnombre", SqlDbType.VarChar);
                //param[0].Value = nombre;
                //param[1] = new SqlParameter("@Papellido", SqlDbType.VarChar);
                //param[1].Value = apellido;
                //param[2] = new SqlParameter("@contra", SqlDbType.VarChar);
                //param[2].Value = contra;
                //param[3] = new SqlParameter("@direccion", SqlDbType.VarChar);
                //param[3].Value = dir;
                //param[4] = new SqlParameter("@telefono", SqlDbType.Char);
                //param[4].Value = tel;

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = spAgregar;
                cmd.Connection = connect;
                cmd.Parameters.AddRange(campos);

                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(ds);
            }
            catch (Exception)
            {

                MessageBox.Show("Error en la insercion");
                return;
            }

        }

        public void editar(DataGridView GridView1, int id, String tel, String dir, string spEditar, SqlParameter[] campos)
        {
            SqlCommand cmd = new SqlCommand();

            //SqlParameter[] param = new SqlParameter[3];
            //param[0] = new SqlParameter("@id", SqlDbType.Int);
            //param[0].Value = id;
            //param[1] = new SqlParameter("@tel", SqlDbType.Char);
            //param[1].Value = tel;
            //param[2] = new SqlParameter("@dir", SqlDbType.VarChar);
            //param[2].Value = dir;


            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = spEditar;
            cmd.Connection = connect;
            cmd.Parameters.AddRange(campos);

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            da.Fill(ds);

        }

        public void eliminar(string spEliminar, SqlParameter[] campos, int id)
        {
            SqlCommand cmd = new SqlCommand();

            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@id", SqlDbType.Int);
            param[0].Value = id;


            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = spEliminar;
            cmd.Connection = connect;
            cmd.Parameters.AddRange(campos);

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            da.Fill(ds);
        }

        public DataTable cargarValores(string sp)
        {
            SqlCommand cmd = new SqlCommand();

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = sp;
            cmd.Connection = connect;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            DataTable dt = new DataTable();
            da.Fill(dt);

            return dt;
        }
    }
}
