using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TareaCurso
{
    public partial class FrmSegurosVida : Form
    {
        public FrmSegurosVida()
        {
            InitializeComponent();
        }

        private void BtnColocar_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();

        }

        private void GroupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void FrmSegurosVida_Load(object sender, EventArgs e)
        {

            string cmd = string.Format("exec ListarVida");
            DataSet ds = Utilidades.Ejecutar(cmd);
            dataGridView1.DataSource = ds.Tables[0];
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            FrmRegistroSVida vi = new FrmRegistroSVida();
            vi.Show();
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            string cmd = string.Format("exec ListarVida");
            DataSet ds = Utilidades.Ejecutar(cmd);
            dataGridView1.DataSource = ds.Tables[0];
        }

        private void DataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            txtpeso.Text = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[2].Value.ToString();
            txtenferme.Text= dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[4].Value.ToString();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            string id = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();
            string cmd = string.Format("exec ActualizarVida '{0}','{1}','{2}'",
                id, txtpeso.Text,txtenferme.Text);
            Utilidades.Ejecutar(cmd);
            MessageBox.Show("Se Actualizó correctamente","INFORMACION");
            string cmd2 = string.Format("exec ListarVida");
            DataSet ds = Utilidades.Ejecutar(cmd2);
            dataGridView1.DataSource = ds.Tables[0];
        }
    }
}
