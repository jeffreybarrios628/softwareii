using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TareaCurso
{
    public partial class FrmBeneficiario : Form
    {
        public FrmBeneficiario()
        {
            InitializeComponent();
        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            FrmRegistroBeneficiario ben = new FrmRegistroBeneficiario();
            ben.Show();

        }

        private void Btnselect_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void FrmBeneficiario_Load(object sender, EventArgs e)
        {
            string cmd = "exec ListarBeneficiarios";
             DataSet ds= Utilidades.Ejecutar(cmd);
            dataGridView1.DataSource = ds.Tables[0];
        }

        private void Btnactabla_Click(object sender, EventArgs e)
        {
            string cmd = "exec ListarBeneficiarios";
            DataSet ds = Utilidades.Ejecutar(cmd);
            dataGridView1.DataSource = ds.Tables[0];

        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            try
            {

                DialogResult res = MessageBox.Show("¿Seguro que desea eliminar a este usuario?", "Atención",
                   MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (res == DialogResult.Yes)
                {
                    int fila = Convert.ToInt32(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value.ToString());
                    string cmd = string.Format("exec EliminarBeneficiario '{0}'", fila);
                    Utilidades.Ejecutar(cmd);
                    MessageBox.Show("Eliminado con éxito", "Completado");
                    cmd = string.Format("exec ListarBeneficiarios");
                    DataSet ds = Utilidades.Ejecutar(cmd);
                    dataGridView1.DataSource = ds.Tables[0];
                }
            }
            catch (Exception error)
            {

                MessageBox.Show("Ocurrió un error ", error.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

        }
    }
}
