using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TareaCurso
{
    public partial class FrmRegistroSVida : Form
    {
        public FrmRegistroSVida()
        {
            InitializeComponent();
        }

        private void FrmRegistroSVida_Load(object sender, EventArgs e)
        {
            txtsuma.Enabled = true;
            txtpeso.Enabled = true;
            txtenferm.Enabled = true;
            txtBene.Enabled = false;
            btnguardar.Enabled = true;
            btnCancel.Enabled = true;
            txtestatura.Enabled = true;
        }

        private void Btnguardarr_Click(object sender, EventArgs e)
        {
            try
            {
                string seleccionado = "";
                foreach (Control contr in gbSexo.Controls)
                {

                    if (contr is RadioButton)
                    {
                        RadioButton rad = contr as RadioButton;
                        if (rad.Checked)
                        {

                            seleccionado = rad.Text;
                            break;

                        }

                    }
                }

                string cmd = string.Format("exec AgregarVida '{0}','{1}','{2}','{3}','{4}','{5}'",
                   seleccionado,txtpeso.Text,txtestatura.Text,txtenferm.Text,txtsuma.Text,txtBene.Text );
                Utilidades.Ejecutar(cmd);
                MessageBox.Show("Se agregó correctamente", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception error)
            {
                MessageBox.Show("Error al guardar", error.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void BtnBuscarAseg_Click(object sender, EventArgs e)
        {

            FrmBeneficiario cl = new FrmBeneficiario();
            DialogResult = DialogResult.OK;

            cl.ShowDialog();
            if (cl.DialogResult == DialogResult.OK)
            {
                txtBene.Text = cl.dataGridView1.Rows[cl.dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();
                
            }


        }

        private void Txtpeso_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == ('.')))
            {
                MessageBox.Show("Sólo se permiten números", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void Txtestatura_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == ('.')))
            {
                MessageBox.Show("Sólo se permiten números", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void Txtsuma_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == ('.')))
            {
                MessageBox.Show("Sólo se permiten números", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }
    }
}
