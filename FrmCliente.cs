using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TareaCurso
{
    public partial class FrmCliente : Form
    {
        private Conection connect;
        int fila;

        public FrmCliente()
        {
            InitializeComponent();
        }

        public FrmCliente(Conection connect)
        {
            this.connect = connect;
            InitializeComponent();
            ListarClientes();
        }

        private void ListarClientes()
        {
            connect.listar(dataGridView1, "VerClientes");
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            fila = e.RowIndex;
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            txtEstado.Text = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[6].Value.ToString();
            txtDireccion.Text = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[7].Value.ToString();
            msktele.Text = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[10].Value.ToString();
            mskCel.Text = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[9].Value.ToString();
           txtLTrabajo.Text = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[19].Value.ToString();
            txtCorreo.Text = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[11].Value.ToString();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            FrmRegistroCliente frc = new FrmRegistroCliente();
            frc.ShowDialog();
        }

        private void GroupBox2_Enter(object sender, EventArgs e)
        {

        }

        public static DataSet fill(string tabla)
        {
            DataSet ds;
            string cmd = string.Format("select * from " + tabla);
            ds = Utilidades.Ejecutar(cmd);
            return ds;

        }

        private void FrmCliente_Load(object sender, EventArgs e)
        {
            string cmd = string.Format("exec ListarClientes");
           DataSet ds= Utilidades.Ejecutar(cmd);
            dataGridView1.DataSource = ds.Tables[0];


        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {

                DataSet ds;

              string  cmd = "select * from Clientes where Primer_Nombre Like ('%" + txtbuscar.Text + "%')";
                ds = Utilidades.Ejecutar(cmd);
                dataGridView1.DataSource = ds.Tables[0];


            }
            catch (Exception error)
            {
                MessageBox.Show("Ocurrió un error ", error.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {


            try
            {

                DialogResult res = MessageBox.Show("¿Seguro que desea eliminar a este usuario?", "Atención",
                   MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (res == DialogResult.Yes)
                {
                    int fila = Convert.ToInt32(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value.ToString());
                  string  cmd = string.Format("exec EliminarCliente '{0}'", fila);
                    Utilidades.Ejecutar(cmd);
                    MessageBox.Show("Eliminado con éxito", "Completado");
                    cmd = string.Format("exec ListarClientes");
                    DataSet   ds = Utilidades.Ejecutar(cmd);
                    dataGridView1.DataSource = ds.Tables[0];
                }
            }
            catch (Exception error)
            {

                MessageBox.Show("Ocurrió un error ", error.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }


        }

        private void Button1_Click(object sender, EventArgs e)
        {

            if (dataGridView1.CurrentRow.Index < 0)
            {

                MessageBox.Show("Debe seleccionar un cliente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }else
            {
                PolizasContratadas pc = new PolizasContratadas();
                pc.Show();

                pc.lblPN.Text = Convert.ToString(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[1].Value);
               // string pn = Convert.ToString(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[1].Value);

                pc.lblSN.Text = Convert.ToString(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[2].Value);
                pc.lblPA.Text = Convert.ToString(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[3].Value);
                pc.lblSA.Text = Convert.ToString(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[4].Value);

                string cmd = string.Format("exec cliente_poliz '{0}'", pc.lblPN.Text);
                DataSet ds = Utilidades.Ejecutar(cmd);
                pc.dataGridView1.DataSource = ds.Tables[0];

            }
        }
       
        private void BtnColocar_Click(object sender, EventArgs e)
        {
            
                

                DialogResult = DialogResult.OK;
                Close();
            
        }

        private void BtnEditar_Click(object sender, EventArgs e)
        {
            string id = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();
            string cmd = string.Format("exec ActualizarCliente '{0}','{1}','{2}','{3}','{4}','{5}','{6}'",
               id, txtDireccion.Text, txtEstado.Text, mskCel.Text, msktele.Text, txtCorreo.Text, txtLTrabajo.Text);

            Utilidades.Ejecutar(cmd);
            MessageBox.Show("Se Actualizó correctamente");
            string cmd1 = string.Format("exec ListarClientes");
            DataSet ds = Utilidades.Ejecutar(cmd1);
            dataGridView1.DataSource = ds.Tables[0];
        }

        private void BtnAgregar_Click_1(object sender, EventArgs e)
        {
            FrmRegistroCliente cl = new FrmRegistroCliente();
            cl.Show();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            string cmd1 = string.Format("exec ListarClientes");
            DataSet ds = Utilidades.Ejecutar(cmd1);
            dataGridView1.DataSource = ds.Tables[0];
        }
    }
}
