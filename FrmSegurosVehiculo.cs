using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TareaCurso
{
    public partial class FrmSegurosVehiculo : Form
    {
        public FrmSegurosVehiculo()
        {
            InitializeComponent();
        }

        private void BtnColocar_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentRow == null)
            {
                MessageBox.Show("Debe selecionar un registro ", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
            {
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        private void FrmSegurosVehiculo_Load(object sender, EventArgs e)
        {

            string cmd = string.Format("exec ListarVehiculos");
            DataSet ds = Utilidades.Ejecutar(cmd);
            dataGridView1.DataSource = ds.Tables[0];
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            FrmRegistroSVehiculo frv = new FrmRegistroSVehiculo();
            frv.Show();
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            string cmd = string.Format("exec ListarVehiculos");
            DataSet ds = Utilidades.Ejecutar(cmd);
            dataGridView1.DataSource = ds.Tables[0];
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            try
            {

                DialogResult res = MessageBox.Show("¿Seguro que desea eliminar este registro?", "Atención",
                   MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (res == DialogResult.Yes)
                {
                    int fila = Convert.ToInt32(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value.ToString());
                    string cmd = string.Format("exec EliminarVehiculo '{0}'", fila);
                    Utilidades.Ejecutar(cmd);
                    MessageBox.Show("Eliminado con éxito", "Completado");
                    cmd = string.Format("exec ListarVehiculos");
                    DataSet ds = Utilidades.Ejecutar(cmd);
                    dataGridView1.DataSource = ds.Tables[0];
                }
            }
            catch (Exception error)
            {

                MessageBox.Show("Ocurrió un error ", error.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }
    }
}
