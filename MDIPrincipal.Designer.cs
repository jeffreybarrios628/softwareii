namespace TareaCurso
{
    partial class MDIPrincipal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MDIPrincipal));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.miClientes = new System.Windows.Forms.ToolStripMenuItem();
            this.cuentaUsuarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miAdminUsuarios = new System.Windows.Forms.ToolStripMenuItem();
            this.miProductos = new System.Windows.Forms.ToolStripMenuItem();
            this.seguroDeVehiculoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.seguroDeViviendaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.seguroDeVidaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aseguradosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.polizasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.newToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.openToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.printToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.printPreviewToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.helpToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.menuStrip.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miClientes,
            this.cuentaUsuarioToolStripMenuItem,
            this.miAdminUsuarios,
            this.miProductos,
            this.aseguradosToolStripMenuItem,
            this.polizasToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(632, 25);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "MenuStrip";
            // 
            // miClientes
            // 
            this.miClientes.Name = "miClientes";
            this.miClientes.Size = new System.Drawing.Size(65, 21);
            this.miClientes.Text = "Clientes";
            this.miClientes.Click += new System.EventHandler(this.MiClientes_Click);
            // 
            // cuentaUsuarioToolStripMenuItem
            // 
            this.cuentaUsuarioToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.cuentaUsuarioToolStripMenuItem.Name = "cuentaUsuarioToolStripMenuItem";
            this.cuentaUsuarioToolStripMenuItem.Size = new System.Drawing.Size(109, 21);
            this.cuentaUsuarioToolStripMenuItem.Text = "Cuenta Usuario";
            // 
            // miAdminUsuarios
            // 
            this.miAdminUsuarios.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.miAdminUsuarios.Name = "miAdminUsuarios";
            this.miAdminUsuarios.Size = new System.Drawing.Size(142, 21);
            this.miAdminUsuarios.Text = "Administrar Usuarios";
            this.miAdminUsuarios.Click += new System.EventHandler(this.miAdminUsuarios_Click);
            // 
            // miProductos
            // 
            this.miProductos.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.seguroDeVehiculoToolStripMenuItem,
            this.seguroDeViviendaToolStripMenuItem,
            this.seguroDeVidaToolStripMenuItem});
            this.miProductos.Name = "miProductos";
            this.miProductos.Size = new System.Drawing.Size(130, 21);
            this.miProductos.Text = "Bienes asegurados";
            this.miProductos.Click += new System.EventHandler(this.MiProductos_Click);
            // 
            // seguroDeVehiculoToolStripMenuItem
            // 
            this.seguroDeVehiculoToolStripMenuItem.Name = "seguroDeVehiculoToolStripMenuItem";
            this.seguroDeVehiculoToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.seguroDeVehiculoToolStripMenuItem.Text = "Vehiculos";
            this.seguroDeVehiculoToolStripMenuItem.Click += new System.EventHandler(this.SeguroDeVehiculoToolStripMenuItem_Click);
            // 
            // seguroDeViviendaToolStripMenuItem
            // 
            this.seguroDeViviendaToolStripMenuItem.Name = "seguroDeViviendaToolStripMenuItem";
            this.seguroDeViviendaToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.seguroDeViviendaToolStripMenuItem.Text = "Viviendas";
            this.seguroDeViviendaToolStripMenuItem.Click += new System.EventHandler(this.SeguroDeViviendaToolStripMenuItem_Click);
            // 
            // seguroDeVidaToolStripMenuItem
            // 
            this.seguroDeVidaToolStripMenuItem.Name = "seguroDeVidaToolStripMenuItem";
            this.seguroDeVidaToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.seguroDeVidaToolStripMenuItem.Text = "Vida";
            this.seguroDeVidaToolStripMenuItem.Click += new System.EventHandler(this.SeguroDeVidaToolStripMenuItem_Click);
            // 
            // aseguradosToolStripMenuItem
            // 
            this.aseguradosToolStripMenuItem.Name = "aseguradosToolStripMenuItem";
            this.aseguradosToolStripMenuItem.Size = new System.Drawing.Size(90, 21);
            this.aseguradosToolStripMenuItem.Text = "Asegurados";
            this.aseguradosToolStripMenuItem.Click += new System.EventHandler(this.AseguradosToolStripMenuItem_Click);
            // 
            // polizasToolStripMenuItem
            // 
            this.polizasToolStripMenuItem.Name = "polizasToolStripMenuItem";
            this.polizasToolStripMenuItem.Size = new System.Drawing.Size(60, 21);
            this.polizasToolStripMenuItem.Text = "Polizas";
            this.polizasToolStripMenuItem.Click += new System.EventHandler(this.PolizasToolStripMenuItem_Click);
            // 
            // toolStrip
            // 
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripButton,
            this.openToolStripButton,
            this.saveToolStripButton,
            this.toolStripSeparator1,
            this.printToolStripButton,
            this.printPreviewToolStripButton,
            this.toolStripSeparator2,
            this.helpToolStripButton});
            this.toolStrip.Location = new System.Drawing.Point(0, 25);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(632, 25);
            this.toolStrip.TabIndex = 1;
            this.toolStrip.Text = "ToolStrip";
            // 
            // newToolStripButton
            // 
            this.newToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripButton.Image")));
            this.newToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.newToolStripButton.Name = "newToolStripButton";
            this.newToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.newToolStripButton.Text = "Nuevo";
            this.newToolStripButton.Click += new System.EventHandler(this.ShowNewForm);
            // 
            // openToolStripButton
            // 
            this.openToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripButton.Image")));
            this.openToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.openToolStripButton.Text = "Abrir";
            this.openToolStripButton.Click += new System.EventHandler(this.OpenFile);
            // 
            // saveToolStripButton
            // 
            this.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripButton.Image")));
            this.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.saveToolStripButton.Text = "Guardar";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // printToolStripButton
            // 
            this.printToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.printToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("printToolStripButton.Image")));
            this.printToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.printToolStripButton.Name = "printToolStripButton";
            this.printToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.printToolStripButton.Text = "Imprimir";
            // 
            // printPreviewToolStripButton
            // 
            this.printPreviewToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.printPreviewToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("printPreviewToolStripButton.Image")));
            this.printPreviewToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.printPreviewToolStripButton.Name = "printPreviewToolStripButton";
            this.printPreviewToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.printPreviewToolStripButton.Text = "Vista previa de impresión";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // helpToolStripButton
            // 
            this.helpToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.helpToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("helpToolStripButton.Image")));
            this.helpToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.helpToolStripButton.Name = "helpToolStripButton";
            this.helpToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.helpToolStripButton.Text = "Ayuda";
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 431);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(632, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "StatusStrip";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(48, 17);
            this.toolStripStatusLabel.Text = "Estado";
            // 
            // MDIPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 453);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.toolStrip);
            this.Controls.Add(this.menuStrip);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MDIPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MDIPrincipal";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MDIPrincipal_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion


        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolStripButton newToolStripButton;
        private System.Windows.Forms.ToolStripButton openToolStripButton;
        private System.Windows.Forms.ToolStripButton saveToolStripButton;
        private System.Windows.Forms.ToolStripButton printToolStripButton;
        private System.Windows.Forms.ToolStripButton printPreviewToolStripButton;
        private System.Windows.Forms.ToolStripButton helpToolStripButton;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStripMenuItem miClientes;
        private System.Windows.Forms.ToolStripMenuItem miProductos;
        private System.Windows.Forms.ToolStripMenuItem cuentaUsuarioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem seguroDeVehiculoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem seguroDeViviendaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem seguroDeVidaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miAdminUsuarios;
        private System.Windows.Forms.ToolStripMenuItem aseguradosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem polizasToolStripMenuItem;
    }
}



