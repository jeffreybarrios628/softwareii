using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TareaCurso
{
    public partial class FrmRegistroSVehiculo : Form
    {
        public FrmRegistroSVehiculo()
        {
            InitializeComponent();
        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                string cmd = string.Format("exec AgregarVehiculo '{0}','{1}','{2}','{3}','{4}','{5}','{6}'," +
                    "'{7}','{8}','{9}','{10}','{11}'", txtMarca.Text, txtModelo.Text, mskAño.Text, cmbUso.SelectedItem.ToString(),
                    txtChasis.Text, txtMotor.Text, txtPasajeros.Text, txtColor.Text, txtPlaca.Text, txtmonto.Text,
                    txtbene.Text,txtentidad.Text);
                Utilidades.Ejecutar(cmd);
                MessageBox.Show("Se agregó correctamente", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception error)
            {
                MessageBox.Show("Error al guardar", error.Message,MessageBoxButtons.OK,MessageBoxIcon.Error);

            }

         }

        private void Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void FrmRegistroSVehiculo_Load(object sender, EventArgs e)
        {
            txtmonto.Enabled = true;
            txtModelo.Enabled = true;
            txtMarca.Enabled = true;
            txtMotor.Enabled = true;
            txtPasajeros.Enabled = true;
            txtPlaca.Enabled = true;
            txtColor.Enabled = true;
            txtChasis.Enabled = true;
            cmbUso.Enabled = true;
            btnAgregar.Enabled = true;
            btnCanc.Enabled = true;
            txtentidad.Enabled = true;
        }

        private void Txtentidad_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void TxtColor_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void Txtentidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {

                MessageBox.Show("Sólo se permiten letras", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void TxtColor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {

                MessageBox.Show("Sólo se permiten letras", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void Txtmonto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == ('.')))
            {
                MessageBox.Show("Sólo se permiten números", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void TxtPasajeros_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)))
            {
                MessageBox.Show("Sólo se permiten números", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }
    }
}
