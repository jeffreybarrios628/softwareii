using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TareaCurso
{
    public partial class MDIPrincipal : Form
    {
        public Conection connect;

        private int childFormNumber = 0;

        public MDIPrincipal()
        {
            InitializeComponent();
        }

        public MDIPrincipal(Conection connect)
        {
            this.connect = connect;
            InitializeComponent();
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            //Form childForm = new Form();
            //childForm.MdiParent = this;
            //childForm.Text = "Ventana " + childFormNumber++;
            //childForm.Show();
        }

        private void OpenFile(object sender, EventArgs e)
        {
            //OpenFileDialog openFileDialog = new OpenFileDialog();
            //openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            //openFileDialog.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*";
            //if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            //{
            //    string FileName = openFileDialog.FileName;
            //}
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //SaveFileDialog saveFileDialog = new SaveFileDialog();
            //saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            //saveFileDialog.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*";
            //if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            //{
            //    string FileName = saveFileDialog.FileName;
            //}
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void verClientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmCliente fc = new FrmCliente();
            fc.MdiParent = this;
            fc.Show();
        }

        private void agregarToolStripMenuItem1_Click(object sender, EventArgs e)
        { 
            FrmRegistroCliente frc = new FrmRegistroCliente();
            frc.MdiParent = this;
            frc.Show();
        }

        private void verPólizasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmPóliza fp = new FrmPóliza();
            fp.MdiParent = this;
            fp.Show();
        }

        private void agregarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmRegistroPóliza frm = new FrmRegistroPóliza();
            frm.MdiParent = this;
            frm.Show();
        }

        private void miASVehiculo_Click(object sender, EventArgs e)
        {
            FrmRegistroSVehiculo frm = new FrmRegistroSVehiculo();
            frm.MdiParent = this;
            frm.Show();
        }

        private void miASVivienda_Click(object sender, EventArgs e)
        {
            FrmRegistroSHogar frm = new FrmRegistroSHogar();
            frm.MdiParent = this;
            frm.Show();
        }

        private void miASVida_Click(object sender, EventArgs e)
        {
            FrmRegistroSVida frm = new FrmRegistroSVida();
            frm.MdiParent = this;
            frm.Show();
        }

        private void miVerSVehiculo_Click(object sender, EventArgs e)
        {
            FrmSegurosVehiculo frm = new FrmSegurosVehiculo();
            frm.MdiParent = this;
            frm.Show();
        }

        private void miVerSVivienda_Click(object sender, EventArgs e)
        {
            FrmSegurosHogar frm = new FrmSegurosHogar();
            frm.MdiParent = this;
            frm.Show();
        }

        private void miVerSVida_Click(object sender, EventArgs e)
        {
            FrmSegurosVida frm = new FrmSegurosVida();
            frm.MdiParent = this;
            frm.Show();
        }

        private void miAdminUsuarios_Click(object sender, EventArgs e)
        {
            FrmRegistroUsuarios frm = new FrmRegistroUsuarios();
            frm.MdiParent = this;
            frm.Show();
        }

        private void MiProductos_Click(object sender, EventArgs e)
        {

        }

        private void AgregarToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            FrmRegistroAsegurado aseg = new FrmRegistroAsegurado();
            aseg.Show();

        }

        private void VerAseguradosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmAsegurados ass = new FrmAsegurados();
            ass.MdiParent = this;
            ass.Show();

        }

        private void PolizasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmPóliza poli = new FrmPóliza();
            poli.MdiParent = this;
            poli.Show();
        }

        private void AseguradosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmAsegurados aseg = new FrmAsegurados();
            aseg.MdiParent = this;
            aseg.Show();
        }

        private void MiClientes_Click(object sender, EventArgs e)
        {
            FrmCliente cl = new FrmCliente();
            cl.MdiParent = this;
            cl.Show();
        }

        private void SeguroDeVehiculoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSegurosVehiculo sh = new FrmSegurosVehiculo();
            sh.MdiParent = this;
            sh.Show();
        }

        private void SeguroDeViviendaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSegurosHogar hog = new FrmSegurosHogar();
            hog.MdiParent = this;
            hog.Show();
        }

        private void SeguroDeVidaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSegurosVida vid = new FrmSegurosVida();
            vid.MdiParent = this;
            vid.Show();
        }

        private void MDIPrincipal_Load(object sender, EventArgs e)
        {

        }
    }
}
