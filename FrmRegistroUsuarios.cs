using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TareaCurso
{
    public partial class FrmRegistroUsuarios : Form
    {
        DataSet ds;
        string cmd;
        public FrmRegistroUsuarios()
        {
            InitializeComponent();
        }

        private void FrmRegistroUsuarios_Load(object sender, EventArgs e)
        {
            txtusuario.Enabled = true;
            txtcontra.Enabled = true;
            cmbrol.Enabled = true;
            cmbp1.Enabled = true;
           
          

            string cmd = string.Format("exec ListarUsuario");
             ds = Utilidades.Ejecutar(cmd);
            dataGridView1.DataSource = ds.Tables[0];

        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            if (txtusuario.Text.Equals("") || txtcontra.Text.Equals("") ||
                cmbrol.SelectedItem.ToString().Equals(""))
            {
                MessageBox.Show("Campos vacíos,", "ERROR    ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            try
            {

                if (cmbp1.Text == "")
            {

                cmbp1.Text = " ";
                cmd = string.Format("exec AgregarUsuario '{0}','{1}','{2}','{3}'",
                txtusuario.Text, txtcontra.Text, cmbrol.SelectedItem.ToString(),
                cmbp1.Text);
                Utilidades.Ejecutar(cmd);
                MessageBox.Show("Se agregó correctamente", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cmd = string.Format("exec ListarUsuario");
                    ds = Utilidades.Ejecutar(cmd);
                    dataGridView1.DataSource = ds.Tables[0];

                }
            else
            {
                cmd = string.Format("exec AgregarUsuario '{0}','{1}','{2}','{3}'",
                txtusuario.Text, txtcontra.Text, cmbrol.SelectedItem.ToString(),
                cmbp1.SelectedItem.ToString());
                Utilidades.Ejecutar(cmd);
                MessageBox.Show("Se agregó correctamente", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cmd = string.Format("exec ListarUsuario");
                    ds = Utilidades.Ejecutar(cmd);
                    dataGridView1.DataSource = ds.Tables[0];
                }
            }
            catch (Exception error)
            {

                MessageBox.Show("Ocurrió un error ", error.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }


        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            try
            {

                DialogResult res = MessageBox.Show("¿Seguro que desea eliminar a este usuario?", "Atención",
                   MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (res == DialogResult.Yes)
                {
                    int fila = Convert.ToInt32(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value.ToString());
                    cmd = string.Format("exec EliminarUser '{0}'", fila);
                    Utilidades.Ejecutar(cmd);
                    MessageBox.Show("Eliminado con éxito", "Completado");
                    cmd = string.Format("exec ListarUsuario");
                    ds = Utilidades.Ejecutar(cmd);
                    dataGridView1.DataSource = ds.Tables[0];
                }
            }
            catch (Exception error)
            {

                MessageBox.Show("Ocurrió un error ", error.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }


        }

        private void Txtbuscar_TextChanged(object sender, EventArgs e)
        {

            try
            {

                DataSet ds;

                 cmd = "select * from Users where Usuario Like ('%" + txtbuscar.Text + "%')";
                ds = Utilidades.Ejecutar(cmd);
                dataGridView1.DataSource = ds.Tables[0];


            }
            catch(Exception error)
            {
                MessageBox.Show("Ocurrió un error ", error.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

        }
    }
}
