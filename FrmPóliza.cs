using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TareaCurso.Reportes;

namespace TareaCurso
{
    public partial class FrmPóliza : Form
    {
        private Conection connect;
      
        
       
        public FrmPóliza()
        {
            InitializeComponent();
           

        }

        public FrmPóliza(Conection connect)
        {
            this.connect = connect;
            InitializeComponent();
            ListarPolizas();       
        }

        private void ListarPolizas()
        {
            connect.listar(dataGridView1, "VerPolizas");
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            FrmRegistroPóliza frp = new FrmRegistroPóliza();
            frp.ShowDialog();
        }

        private void FrmPóliza_Load(object sender, EventArgs e)
        {
            string cmd = string.Format("exec ListarPolizas");
            DataSet ds= Utilidades.Ejecutar(cmd);
            dataGridView1.DataSource = ds.Tables[0];
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            FrmRegistroPóliza pl = new FrmRegistroPóliza();
            pl.Show();
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            string cmd1 = string.Format("exec ListarPolizas");
            DataSet ds = Utilidades.Ejecutar(cmd1);
            dataGridView1.DataSource = ds.Tables[0];
        }

        private void BtnGenerar_Click(object sender, EventArgs e)
        {
           // capturar el campo correspondiente a Numero de Poliza y extraer  las dos primeras
            //letras (AU o VI o  VG)
            string op = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[1].Value.ToString();
            string elec = op.Substring(0, 2);


            if (elec == "AU")
            {

                int fila = Convert.ToInt32(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value.ToString());

                string cmd2 = string.Format("exec ActualizarFac");
                DataSet dss = Utilidades.Ejecutar(cmd2);
                string numfac = dss.Tables[0].Rows[0]["NumFac"].ToString().Trim();


                string cmd = string.Format("exec RecogerDatosVehiculo " + fila);
                DataSet ds = Utilidades.Ejecutar(cmd);
                ReporteVehiculo rp = new ReporteVehiculo();
                rp.reportViewer1.LocalReport.DataSources[0].Value = ds.Tables[0];
                rp.ShowDialog();

            }
             else if(elec=="VI")
            {

                int fila = Convert.ToInt32(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value.ToString());

                string cmd2 = string.Format("exec ActualizarFac");
                DataSet dss = Utilidades.Ejecutar(cmd2);
                string numfac = dss.Tables[0].Rows[0]["NumFac"].ToString().Trim();


                string cmd = string.Format("exec RecogerDatosHogar " + fila);
                DataSet ds = Utilidades.Ejecutar(cmd);
                ReporteVivienda rp = new ReporteVivienda();
                rp.reportViewer1.LocalReport.DataSources[0].Value = ds.Tables[0];
                rp.ShowDialog();

            }
            else
            {
                int fila = Convert.ToInt32(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value.ToString());

                string cmd2 = string.Format("exec ActualizarFac");
                DataSet dss = Utilidades.Ejecutar(cmd2);
                string numfac = dss.Tables[0].Rows[0]["NumFac"].ToString().Trim();

                string cmd = string.Format("exec RecogerDatosVida " + fila);
                DataSet ds = Utilidades.Ejecutar(cmd);
                ReporteVida rp = new ReporteVida();
                rp.reportViewer1.LocalReport.DataSources[0].Value = ds.Tables[0];
                rp.ShowDialog();


            }


        }

        private void Button3_Click(object sender, EventArgs e)
        {

            try
            {

                DialogResult res = MessageBox.Show("¿Seguro que desea eliminar a este usuario?", "Atención",
                   MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (res == DialogResult.Yes)
                {
                    int fila = Convert.ToInt32(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value.ToString());
                    string cmd = string.Format("exec EliminarPoliza '{0}'", fila);
                    Utilidades.Ejecutar(cmd);
                    MessageBox.Show("Eliminado con éxito", "Completado");
                    cmd = string.Format("exec ListarPolizas");
                    DataSet ds = Utilidades.Ejecutar(cmd);
                    dataGridView1.DataSource = ds.Tables[0];
                }
            }
            catch (Exception error)
            {

                MessageBox.Show("Ocurrió un error ", error.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }
    }
}
